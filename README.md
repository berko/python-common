# __python-common__


This repository contains the common python files that are
part of the [NOMAD Laboratory](http://nomad-lab.eu).
The official version lives at

    git@gitlab.mpcdf.mpg.de:nomad-lab/python-common.git

you can browse it at

    https://gitlab.mpcdf.mpg.de/nomad-lab/python-common

Some things rely on having the nomad-meta-info checked out at the same level.
The simplest way to have this is to check out nomad-lab-base recursively:

    git clone --recursive git@gitlab.mpcdf.mpg.de:nomad-lab/nomad-lab-base.git

then this will be in python-common within it.

# Local Install

The following instructions were tested on Ubuntu 14.04. With these instructions
you can install the package for the local user (doesn't need root privileges).
First make sure you have [pip](https://pypi.python.org/pypi/pip) available. If
not, you can install it for python 2.x with:

```sh
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py --user
```

or for python3 with:

```sh
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user
```

The modules used for parser development are located under the 'nomadcore'
package. If you wish to setup this package locally, you can do so by first
installing the required dependencies (use pip3 for python3 setup)

```sh
pip install -r requirements.txt --user
#pip3 install -r requirements.txt --user
```

and then installing the package itself with the provided installation
script(use python3 for python3 setup)

```sh
python setup.py develop --user
#python3 setup.py develop --user
```

This will install a development version, which means that if you update the
source code, all the changes will be available immediately without reinstall of
the package. The current setup also assumes a certain location for the metainfo
repository. If you place all the repositories (python-common, nomad-meta-info,
parser repository) in the same folder, things should work.

After this the package will be available to import by simply calling

```python
import nomadcore
```

in python. The development mode also means that the latest version of the code
is used, so any updates from git will automatically be available. You can
install a static snapshot by using 'install' instead of 'develop'.
